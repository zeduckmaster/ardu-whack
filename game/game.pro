TEMPLATE = app
CONFIG += c++11
CONFIG -= qt console

SOURCES += game.cpp

HEADERS += \
bitmaps.h \
sounds.h \
zarduboy.h \
main.h

win32 {
INCLUDEPATH += ../SDL/include
LIBS += -L$$PWD/../SDL/win32/
LIBS += -lSDL2 -lSDL2main
}

linux {
LIBS += -lSDL2 -lSDL2main
}

macx {
QMAKE_LFLAGS += -rpath @executable_path/../Frameworks
INCLUDEPATH += /Library/Frameworks/SDL2.framework/Headers
LIBS += -F/Library/Frameworks/ -framework SDL2
}
