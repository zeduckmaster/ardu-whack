![ardu-whack](https://framagit.org/zeduckmaster/ardu-whack/raw/master/screen0.png "ardu-whack screenshot")

# How to play

Whack them all to make the highest score!

![ardu-whack](https://framagit.org/zeduckmaster/ardu-whack/raw/master/screen1.png "ardu-whack screenshot")

Use D-Pad (arrows on desktop) to move the hammer, and A to smash the hammer. On desktop, you can quit using *Escape* key.

# Binaries

* arduboy: [ardu-whack-1.2.hex](https://acloud.zaclys.com/index.php/s/HHToHd7cKL3bGX3?path=%2Fardu-whack) + [ardu-whack-1.2.arduboy](https://acloud.zaclys.com/index.php/s/HHToHd7cKL3bGX3?path=%2Fardu-whack)
* windows 64 bits: [ardu-whack-1.2.7z](https://acloud.zaclys.com/index.php/s/HHToHd7cKL3bGX3?path=%2Fardu-whack)
* linux 64 bits (ubuntu 18.04): [ardu-whack-1.2-x86_64.AppImage](https://acloud.zaclys.com/index.php/s/HHToHd7cKL3bGX3?path=%2Fardu-whack)

# How to build

## Arduboy

The game has been compiled with the official Arduino ide 1.8.2 and tested successfuly on Arduboy v1.

## Desktop

The game can be compiled for windows, linux and macos with the provided Qt Creator project.

Building for macos and linux require to install SDL2 framework.

# Information

License: GPLv3: https://www.gnu.org/licenses/gpl-3.0.en.html
